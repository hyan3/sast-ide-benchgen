package baseline

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/dustin/go-humanize"
	sitter "github.com/smacker/go-tree-sitter"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/config"
	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/helper"
)

// Fetch clones a repository, extracts language-specific files, and cleans up temporary data.
func Fetch(appConfig *config.AppConfig) error {
	tempDir, err := os.MkdirTemp("", "baseline_repo_*")
	if err != nil {
		return fmt.Errorf("failed to create temp directory: %v", err)
	}

	repo := helper.Repo{
		Name:     "baseline",
		FullName: "baseline",
		HTMLURL:  appConfig.BaselineRepoURL,
	}
	if err := helper.CloneRepo(repo, tempDir); err != nil {
		return fmt.Errorf("failed to clone repo: %v", err)
	}

	langConfig, err := config.GetLanguageConfig(appConfig.Lang)
	if err != nil {
		return err
	}

	if err := splitRuleFiles(tempDir, appConfig.BaselineSamplesDir, langConfig); err != nil {
		return fmt.Errorf("failed to extract source files: %v", err)
	}

	if err := os.RemoveAll(tempDir); err != nil {
		return fmt.Errorf("failed to remove temp directory: %v", err)
	}

	log.Info("File extraction completed.")
	return nil
}

// splits rule files of the specified language `lang` in `srcDir`, creating individual files in `destDir` with one vulnerability each.
func splitRuleFiles(srcDir, destDir string, langConfig config.LanguageConfig) error {
	if err := os.MkdirAll(destDir, os.ModePerm); err != nil {
		return fmt.Errorf("failed to create download directory: %v", err)
	}

	// optimization: instantiate the tree-sitter parser only once, and re-use it each time we need to process a file,
	// rather than instantiating a new parser for every file.
	parser := sitter.NewParser()
	parser.SetLanguage(langConfig.Language())
	err := filepath.Walk(srcDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return fmt.Errorf("failed to walk dir: %v", err)
		}

		if info.IsDir() {
			return nil
		}

		fileBasename := filepath.Base(path)
		matched := strings.HasPrefix(fileBasename, "rule-") && strings.HasSuffix(fileBasename, langConfig.Extension())

		// not a rule file, skip it
		if !matched {
			return nil
		}

		return splitFileIntoSingleVulnFiles(path, destDir, langConfig, parser)
	})
	if err != nil {
		return fmt.Errorf("failed to copy rule files: %v", err)
	}

	return nil
}

// Split the file into multiple files, each containing only one vuln
func splitFileIntoSingleVulnFiles(srcPath, dstDir string, langConfig config.LanguageConfig, parser *sitter.Parser) error {
	content, err := os.ReadFile(srcPath)
	if err != nil {
		return fmt.Errorf("failed to read file with path %q: %v", srcPath, err)
	}

	tree, err := parser.ParseCtx(context.Background(), nil, content)
	if err != nil {
		return fmt.Errorf("failed to parse source code with treesitter: %v", err)
	}
	vulnTaggingComments, err := findVulnTaggingComments(tree, content, langConfig)
	if err != nil {
		return fmt.Errorf("failed to find vuln-tagging comments in source code: %v", err)
	}

	if len(vulnTaggingComments) == 0 {
		return nil
	}

	baseName := strings.TrimSuffix(filepath.Base(srcPath), filepath.Ext(srcPath))
	// For each vuln, create a new program with the correct file name
	for i := range vulnTaggingComments {
		newContent, err := commentOutOtherVulns(content, vulnTaggingComments, i, langConfig)
		if err != nil {
			// ignore code that cannot be parsed correctly by treesitter
			continue
		}

		// Create a new file name like "a1.go", "a2.go", "a3.go"
		newFileName := fmt.Sprintf("%s%d%s", baseName, i+1, filepath.Ext(srcPath))
		dstPath := filepath.Join(dstDir, newFileName)
		if err := os.WriteFile(dstPath, newContent, 0640); err != nil {
			return err
		}
	}
	return nil
}

func countVulns(content []byte) int {
	return len(config.RuleidRegex.FindAll(content, -1))
}

func findVulnTaggingComments(tree *sitter.Tree, content []byte, langConfig config.LanguageConfig) ([]*sitter.Node, error) {
	var vulnTaggingComments []*sitter.Node
	query, err := langConfig.CommentQuery()
	if err != nil {
		return nil, err
	}
	queryCursor := sitter.NewQueryCursor()
	queryCursor.Exec(query, tree.RootNode())

	for {
		match, ok := queryCursor.NextMatch()
		if !ok {
			break
		}

		commentNode := match.Captures[0].Node
		commentText := commentNode.Content(content)

		if config.RuleidRegex.MatchString(commentText) {
			vulnTaggingComments = append(vulnTaggingComments, commentNode)
		}
	}
	return vulnTaggingComments, nil
}

// Comment out the statement following each ruleid except the one at activeVulnIndex
func commentOutOtherVulns(content []byte, vulnTaggingComments []*sitter.Node, activeVulnIndex int, langConfig config.LanguageConfig) ([]byte, error) {
	// Split the content into lines for easier processing
	lines := strings.Split(string(content), "\n")
	commentSymbol := langConfig.CommentSymbol()
	for i, vulnTaggingComment := range vulnTaggingComments {
		if i == activeVulnIndex {
			continue
		}
		// Locate the statement node following the comment
		nxtStat := findNextStatementNode(vulnTaggingComment)
		if nxtStat != nil {
			startRow := vulnTaggingComment.StartPoint().Row
			endRow := nxtStat.EndPoint().Row
			for row := startRow; row <= endRow; row++ {
				lines[row] = commentSymbol + " removed by sast-ide-benchgen"
			}
		} else {
			return nil, fmt.Errorf("failed to locate a statement right after the %s vuln tagging comment", humanize.Ordinal(i+1))
		}
	}

	modifiedContent := []byte(strings.Join(lines, "\n"))
	if numVulns := countVulns(modifiedContent); numVulns != 1 {
		return nil, fmt.Errorf("expected exactly 1 vuln, but found %d vulns", numVulns)
	}
	return modifiedContent, nil
}

// FindNextStatementNode finds the next statement node after the commentNode
func findNextStatementNode(commentNode *sitter.Node) *sitter.Node {
	// Get the parent node of the commentNode
	parentNode := commentNode.Parent()

	// Iterate over all children of the parent node
	for i := 0; i < int(parentNode.ChildCount()); i++ {
		child := parentNode.Child(i)

		// Find the current commentNode
		if child.Equal(commentNode) {
			// Once we find the commentNode, look for the next node (statement) after it
			for j := i + 1; j < int(parentNode.ChildCount()); j++ {
				nextSibling := parentNode.Child(j)

				// We assume the next statement node will have a specific type, e.g., "expression_statement", "declaration"

				if isBlockNode(nextSibling.Type()) {
					for i := 0; i < int(nextSibling.ChildCount()); i++ {
						child := nextSibling.Child(i)
						if child != nil && isStatementNode(child.Type()) {
							return child
						}
					}
				} else if isStatementNode(nextSibling.Type()) {
					return nextSibling
				}
			}
		}
	}

	return nil
}

func isStatementNode(nodeType string) bool {
	if strings.HasSuffix(nodeType, "statement") {
		return true
	}
	if strings.HasSuffix(nodeType, "declaration") {
		return true
	}
	if nodeType == "call" || nodeType == "assignment" { //for ruby
		return true
	}
	log.Debugln("node type: ", nodeType)
	return false
}

func isBlockNode(nodeType string) bool {
	return nodeType == "block" || nodeType == "body_statement"
}
