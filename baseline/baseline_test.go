package baseline

import (
	"bytes"
	"context"
	"fmt"
	"path/filepath"
	"testing"

	sitter "github.com/smacker/go-tree-sitter"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/config"
	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/internal/testutils"
)

func TestFindVulnTaggingComments(t *testing.T) {
	tests := []struct {
		lang string
		want int
	}{
		{
			lang: "java",
			want: 4,
		},
		{
			lang: "ruby",
			want: 10,
		},
		{
			lang: "go",
			want: 1,
		},
		{
			lang: "python",
			want: 5,
		},
	}

	for _, tt := range tests {
		t.Run(tt.lang, func(t *testing.T) {
			fixturePath := testutils.FixturePath(t, tt.lang)
			vulnTaggingComments, _, _ := setupTestFindVulnTaggingComments(t, fixturePath, tt.lang)
			require.Len(t, vulnTaggingComments, tt.want)
		})
	}
}

func TestCommentOutOtherVulns(t *testing.T) {
	tests := []struct {
		lang, fixture     string
		want, activeIndex int
		wantErr           string
	}{
		{
			lang:        "java",
			want:        1,
			fixture:     testutils.FixturePath(t, "java"),
			activeIndex: 1,
		},
		{
			lang:    "java",
			want:    1,
			fixture: "testdata/fixtures/TestCommentOutOtherVulns/java/malformed_ruleid.java",
			wantErr: "failed to locate a statement right after the 2nd vuln tagging comment",
		},
		{
			lang:        "ruby",
			want:        1,
			fixture:     testutils.FixturePath(t, "ruby"),
			activeIndex: 1,
		},
		{
			lang: "go",
			// an error is expected because the go fixture contains only one vulnerability, so there are no other vulns to comment
			wantErr:     "expected exactly 1 vuln, but found 0 vulns",
			fixture:     testutils.FixturePath(t, "go"),
			activeIndex: 1,
		},
		{
			lang:    "go",
			want:    1,
			fixture: "testdata/fixtures/TestCommentOutOtherVulns/go/malformed_ruleid.go",
			wantErr: "failed to locate a statement right after the 2nd vuln tagging comment",
		},
		{
			lang:        "python",
			want:        1,
			fixture:     testutils.FixturePath(t, "python"),
			activeIndex: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.lang, func(t *testing.T) {
			vulnTaggingComments, sourceCode, langConfig := setupTestFindVulnTaggingComments(t, tt.fixture, tt.lang)
			newContent, err := commentOutOtherVulns([]byte(sourceCode), vulnTaggingComments, tt.activeIndex, langConfig)
			if tt.wantErr != "" {
				require.EqualError(t, err, tt.wantErr)
			} else {
				require.NoError(t, err)

				target := []byte("ruleid")
				count := bytes.Count(newContent, target)
				require.Equal(t, tt.want, count)
			}
		})
	}
}

func TestFindNextStatementNode(t *testing.T) {
	tests := []struct {
		fixture, lang string
	}{
		{
			fixture: "malformed_ruleid.go",
			lang:    "go",
		},
		{
			fixture: "malformed_ruleid.java",
			lang:    "java",
		},
	}

	for _, tt := range tests {
		t.Run(tt.lang, func(t *testing.T) {
			fixturePath := filepath.Join("testdata/fixtures/TestCommentOutOtherVulns", tt.lang, tt.fixture)
			vulnTaggingComments, _, _ := setupTestFindVulnTaggingComments(t, fixturePath, tt.lang)
			nxtStat := findNextStatementNode(vulnTaggingComments[1])
			require.Nil(t, nxtStat)
		})
	}
}

func TestCountVulns(t *testing.T) {
	tests := []struct {
		sourceCodeFile string
		lang           string
		want           int
	}{
		{
			lang: "java",
			want: 4,
		},
		{
			lang: "ruby",
			want: 10,
		},
		{
			lang: "go",
			want: 1,
		},
		{
			lang: "python",
			want: 5,
		},
	}

	for _, tt := range tests {
		t.Run(tt.lang, func(t *testing.T) {
			fixturePath := testutils.FixturePath(t, tt.lang)
			sourceCode := testutils.ReadFile(t, fixturePath)
			count := countVulns([]byte(sourceCode))
			require.Equal(t, tt.want, count)
		})
	}
}

func TestSplitRuleFiles(t *testing.T) {
	tests := []struct {
		scenario, lang string
	}{
		{
			lang:     "go",
			scenario: "complex",
		},
		{
			lang:     "java",
			scenario: "simple",
		},
		{
			lang:     "java",
			scenario: "complex",
		},
		{
			lang:     "ruby",
			scenario: "complex",
		},
		{
			lang:     "python",
			scenario: "complex",
		},
	}

	for _, tt := range tests {
		t.Run(fmt.Sprintf("%s-%s", tt.lang, tt.scenario), func(t *testing.T) {
			tempDir := testutils.TempDir(t)
			expectDir := filepath.Join("testdata/expectations/TestSplitRuleFiles", tt.lang, tt.scenario)
			fixturesDir := filepath.Join("testdata/fixtures/TestSplitRuleFiles", tt.lang, tt.scenario)

			langConfig, err := config.GetLanguageConfig(tt.lang)
			require.NoError(t, err)

			err = splitRuleFiles(fixturesDir, tempDir, langConfig)
			require.NoError(t, err)

			testutils.CompareDirs(t, expectDir, tempDir)
		})
	}
}

func setupTestFindVulnTaggingComments(t *testing.T, sourceCodeFile, lang string) ([]*sitter.Node, string, config.LanguageConfig) {
	sourceCode := testutils.ReadFile(t, sourceCodeFile)

	langConfig, err := config.GetLanguageConfig(lang)
	require.NoError(t, err)

	parser := sitter.NewParser()
	parser.SetLanguage(langConfig.Language())

	tree, err := parser.ParseCtx(context.Background(), nil, []byte(sourceCode))
	require.NoError(t, err)

	vulnTaggingComments, err := findVulnTaggingComments(tree, []byte(sourceCode), langConfig)
	require.NoError(t, err)

	return vulnTaggingComments, sourceCode, langConfig
}
