package main

import (
	"fmt"
	"os"
)

func writeTempFile(n int) string {
	// ruleid: go_filesystem_rule-tempfiles
	os.WriteFile("/tmp/demo2", []byte("This is some data. Number: "+n), 0644)
	return "Temp file successfully written"
}

func main() {
	n :=
		// ruleid: this rule is malformed because the next line doesn't contain a statement
		10
	fmt.Println(writeTempFile(n))
}
