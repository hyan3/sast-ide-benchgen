public class SumOfSquares {

    public static int square(int n) {
        return n * n;
    }

    public static int sumOfSquares(int n) {
        // ruleid: dummy-rule
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            sum += square(i);
        }
        return sum;
    }

    public static void main(String[] args) {
        int
        // ruleid: dummy-rule
        n = 10;
        System.out.printf("The sum of squares from 1 to %d is: %d\n", n, sumOfSquares(n));
    }
}
