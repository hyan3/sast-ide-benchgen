// ruleid: This is vulnerability 1
public class VulnerableClass1 {
    public void vulnerabilityMethod1() {
        System.out.println("vulnerability 1");
    }
}

// ruleid: This is vulnerability 2
public class VulnerableClass2 {
    public void vulnerabilityMethod2() {
        System.out.println("vulnerability 2");
    }
}

// ruleid: This is vulnerability 3
public class VulnerableClass3 {
    public void vulnerabilityMethod3() {
        System.out.println("vulnerability 3");
    }
}