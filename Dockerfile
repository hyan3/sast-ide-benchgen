FROM golang:1.22.3-alpine AS builder

RUN apk update && apk add --no-cache gcc g++ git make

WORKDIR /app
COPY go.mod ./
RUN go mod tidy
RUN go mod download
COPY config.toml ./

COPY . .
RUN go build -o benchgen ./cmd

FROM alpine:latest

RUN apk update && apk add --no-cache git

WORKDIR /root/

COPY --from=builder /app/config.toml .
COPY --from=builder /app/benchgen .
