package hybrid

import (
	"path/filepath"
	"testing"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/internal/testutils"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/config"

	"github.com/stretchr/testify/require"
)

func TestCombineFiles(t *testing.T) {
	tempDir := testutils.TempDir(t)

	aFile := "testdata/fixtures/TestCombineFiles/go/rule-fileperm1.go"
	bFile := "testdata/fixtures/TestCombineFiles/go/output_0.go"
	got := filepath.Join(tempDir, "output.go")
	want := "testdata/expectations/TestCombineFiles/go/output.go"

	err := combineFiles(aFile, bFile, got)
	require.NoError(t, err)

	testutils.EqualFiles(t, got, want)
}

func TestGenSamples(t *testing.T) {
	ossSamplesDir := "testdata/fixtures/TestGenSamples/go/oss"
	baselineSamplesDir := "testdata/fixtures/TestGenSamples/go/baseline"
	expectationsDir := "testdata/expectations/TestGenSamples/go"
	tempDir := testutils.TempDir(t)

	appConfig := &config.AppConfig{
		Lang:                       "go",
		NumVulnerabilities:         2,
		NumHybridSamplesToGenerate: 3,
		GeneratedOssSamplesDir:     ossSamplesDir,
		BaselineSamplesDir:         baselineSamplesDir,
		GeneratedHybridSamplesDir:  tempDir,
		RandSeed:                   1,
	}

	err := GenSamples(appConfig)
	require.NoError(t, err)

	testutils.CompareDirs(t, expectationsDir, tempDir)
}
