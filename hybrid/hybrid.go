package hybrid

import (
	"fmt"
	"math/rand"
	"os"
	"path/filepath"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/config"
	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/helper"

	log "github.com/sirupsen/logrus"
)

// GenSamples generates hybrid sample files by combining baseline and OSS samples.
func GenSamples(appConfig *config.AppConfig) error {
	if err := os.MkdirAll(appConfig.GeneratedHybridSamplesDir, os.ModePerm); err != nil {
		return fmt.Errorf("failed to create output directory: %v", err)
	}

	langConfig, err := config.GetLanguageConfig(appConfig.Lang)
	if err != nil {
		return err
	}

	langExt := langConfig.Extension()

	baselineSamples, err := helper.GetFilesByExtension(appConfig.BaselineSamplesDir, langExt)
	if err != nil {
		return fmt.Errorf("failed to get baseline sample files with extension %q: %v", langExt, err)
	}

	ossSamples, err := helper.GetFilesByExtension(appConfig.GeneratedOssSamplesDir, langExt)
	if err != nil {
		return fmt.Errorf("failed to get oss sample files with extension %q: %v", langExt, err)
	}

	r := rand.New(rand.NewSource(appConfig.RandSeed))

	// generate a specified number of sample files by concatenating baseline samples and oss samples
	for i := 0; i < appConfig.NumHybridSamplesToGenerate; i++ {
		// use a random oss sample file
		ossSample := ossSamples[r.Intn(len(ossSamples))]
		outputFilePath := filepath.Join(appConfig.GeneratedHybridSamplesDir, fmt.Sprintf("hybrid_%d%s", i, langExt))

		for j := 0; j < appConfig.NumVulnerabilities; j++ {
			// use a random baseline sample file
			baselineSample := baselineSamples[r.Intn(len(baselineSamples))]
			log.Infof("Combining %s and %s into %s", ossSample, baselineSample, outputFilePath)
			if err := combineFiles(ossSample, baselineSample, outputFilePath); err != nil {
				return fmt.Errorf("failed to combine files: %v", err)
			}
			ossSample = outputFilePath
		}
	}

	log.Info("File generation completed.")
	return nil
}

func combineFiles(aFilePath, bFilePath, outputFilePath string) error {
	aContent, err := os.ReadFile(aFilePath)
	if err != nil {
		return err
	}

	bContent, err := os.ReadFile(bFilePath)
	if err != nil {
		return err
	}

	// combine/concat
	combinedContent := append(append(aContent, '\n'), bContent...)

	return os.WriteFile(outputFilePath, combinedContent, 0640)
}
