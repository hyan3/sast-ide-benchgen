func testAdapterOperationWithInvalidConfig(
	t *testing.T,
	name string,
	tc adapterOperationInvalidConfigTestCase,
	adapter *azureAdapter,
	operation func(context.Context) cache.PresignedURL,
) {
	t.Run(name, func(t *testing.T) {
		prepareMockedCredentialsResolverForInvalidConfig(adapter, tc)
		hook := test.NewGlobal()

		u := operation(context.Background())
		assert.Nil(t, u.URL)

		message, err := hook.LastEntry().String()
		require.NoError(t, err)
		assert.Contains(t, message, tc.expectedErrorMsg)
	})
}

func TestCreateAdapter(t *testing.T) {
	adapterMock := new(MockAdapter)

	tests := map[string]factorizeTestCase{
		"adapter doesn't exist": {
			adapter:          nil,
			errorOnFactorize: nil,
			expectedAdapter:  nil,
			expectedError:    `cache factory not found: factory for cache adapter \"test\" was not registered`,
		},
		"adapter exists": {
			adapter:          adapterMock,
			errorOnFactorize: nil,
			expectedAdapter:  adapterMock,
			expectedError:    "",
		},
		"adapter errors on factorize": {
			adapter:          adapterMock,
			errorOnFactorize: errors.New("test error"),
			expectedAdapter:  nil,
			expectedError:    `cache adapter could not be initialized: test error`,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			cleanupFactoriesMap := prepareMockedFactoriesMap()
			defer cleanupFactoriesMap()

			adapterTypeName := "test"

			if test.adapter != nil {
				err := factories.Register(adapterTypeName, makeTestFactory(test))
				assert.NoError(t, err)
			}

			_ = factories.Register(
				"additional-adapter",
				func(config *common.CacheConfig, timeout time.Duration, objectName string) (Adapter, error) {
					return new(MockAdapter), nil
				})

			config := &common.CacheConfig{
				Type: adapterTypeName,
			}

			adapter, err := getCreateAdapter(config, defaultTimeout, "key")

			if test.expectedError == "" {
				assert.NoError(t, err)
			} else {
				assert.Error(t, err)
			}

			assert.Equal(t, test.expectedAdapter, adapter)
		})
	}
}

func testUploadEnvWithInvalidConfig(
	t *testing.T,
	name string,
	tc adapterOperationInvalidConfigTestCase,
	adapter *azureAdapter,
	operation func(context.Context) (map[string]string, error),
) {
	t.Run(name, func(t *testing.T) {
		prepareMockedCredentialsResolverForInvalidConfig(adapter, tc)

		u, err := operation(context.Background())
		assert.NoError(t, err)
		assert.Equal(t, accountName, u["AZURE_STORAGE_ACCOUNT"])
		assert.Equal(t, storageDomain, u["AZURE_STORAGE_DOMAIN"])
		assert.NotContains(t, u, "AZURE_SAS_TOKEN")
	})
}

