func testUploadEnvWithInvalidConfig(
	t *testing.T,
	name string,
	tc adapterOperationInvalidConfigTestCase,
	adapter *azureAdapter,
	operation func(context.Context) (map[string]string, error),
) {
	t.Run(name, func(t *testing.T) {
		prepareMockedCredentialsResolverForInvalidConfig(adapter, tc)

		u, err := operation(context.Background())
		assert.NoError(t, err)
		assert.Equal(t, accountName, u["AZURE_STORAGE_ACCOUNT"])
		assert.Equal(t, storageDomain, u["AZURE_STORAGE_DOMAIN"])
		assert.NotContains(t, u, "AZURE_SAS_TOKEN")
	})
}

func init() {
	err := cache.Factories().Register("azure", New)
	if err != nil {
		panic(err)
	}
}

func init() {
	_, _ = maxprocs.Set()
	memlimit.SetGoMemLimitWithEnv()
}

func init() {
	err := cache.Factories().Register("azure", New)
	if err != nil {
		panic(err)
	}
}

func testAdapterOperation(
	t *testing.T,
	tc adapterOperationTestCase,
	name string,
	expectedMethod string,
	adapter *azureAdapter,
	operation func(context.Context) cache.PresignedURL,
) {
	t.Run(name, func(t *testing.T) {
		cleanupCredentialsResolverMock := prepareMockedCredentialsResolver(adapter)
		defer cleanupCredentialsResolverMock(t)

		prepareMockedSignedURLGenerator(t, tc, expectedMethod, adapter)
		hook := test.NewGlobal()

		u := operation(context.Background())

		if tc.expectedError != "" {
			message, err := hook.LastEntry().String()
			require.NoError(t, err)
			assert.Contains(t, message, tc.expectedError)
			return
		}

		assert.Empty(t, hook.AllEntries())

		assert.Equal(t, tc.returnedURL, u.URL.String())
	})
}

func getCreateAdapter(cacheConfig *common.CacheConfig, timeout time.Duration, objectName string) (Adapter, error) {
	create, err := Factories().Find(cacheConfig.Type)
	if err != nil {
		return nil, fmt.Errorf("cache factory not found: %w", err)
	}

	adapter, err := create(cacheConfig, timeout, objectName)
	if err != nil {
		return nil, fmt.Errorf("cache adapter could not be initialized: %w", err)
	}

	return adapter, nil
}

func TestAdapterOperation_InvalidConfig(t *testing.T) {
	tests := map[string]adapterOperationInvalidConfigTestCase{
		"no-azure-config": {
			containerName:    containerName,
			expectedErrorMsg: "Missing Azure configuration",
		},
		"error-on-credentials-resolver-initialization": {
			provideAzureConfig:                       true,
			errorOnCredentialsResolverInitialization: true,
		},
		"credentials-resolver-resolve-error": {
			provideAzureConfig:              true,
			credentialsResolverResolveError: true,
			containerName:                   containerName,
			expectedErrorMsg:                `error resolving Azure credentials" error="test error"`,
			expectedGoCloudURL:              "azblob://test/key",
		},
		"no-credentials": {
			provideAzureConfig: true,
			containerName:      containerName,
			expectedErrorMsg:   "error creating Azure SAS signer\" error=\"missing Azure storage account name\"",
			expectedGoCloudURL: "azblob://test/key",
		},
		"no-account-name": {
			provideAzureConfig: true,
			accountKey:         accountKey,
			containerName:      containerName,
			expectedErrorMsg:   "error creating Azure SAS signer\" error=\"missing Azure storage account name\"",
			expectedGoCloudURL: "azblob://test/key",
		},
		"no-account-key": {
			provideAzureConfig: true,
			accountName:        accountName,
			containerName:      containerName,
			expectedErrorMsg:   "missing Azure storage account key",
			expectedGoCloudURL: "azblob://test/key",
		},
		"invalid-container-name-and-no-account-key": {
			provideAzureConfig: true,
			accountName:        accountName,
			containerName:      "\x00",
			expectedErrorMsg:   "missing Azure storage account key",
		},
		"container-not-specified": {
			provideAzureConfig: true,
			accountName:        "access-id",
			accountKey:         accountKey,
			expectedErrorMsg:   "ContainerName can't be empty",
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			cleanupCredentialsResolverInitializerMock := prepareMockedCredentialsResolverInitializer(tc)
			defer cleanupCredentialsResolverInitializerMock()

			config := defaultAzureCache()
			config.Azure.ContainerName = tc.containerName
			if !tc.provideAzureConfig {
				config.Azure = nil
			}

			a, err := New(config, defaultTimeout, objectName)
			if !tc.provideAzureConfig {
				assert.Nil(t, a)
				assert.EqualError(t, err, "missing Azure configuration")
				return
			}

			if tc.errorOnCredentialsResolverInitialization {
				assert.Nil(t, a)
				assert.EqualError(t, err, "error while initializing Azure credentials resolver: test error")
				return
			}

			require.NotNil(t, a)
			assert.NoError(t, err)

			adapter, ok := a.(*azureAdapter)
			require.True(t, ok, "Adapter should be properly casted to *adapter type")

			testAdapterOperationWithInvalidConfig(t, "GetDownloadURL", tc, adapter, a.GetDownloadURL)
			testAdapterOperationWithInvalidConfig(t, "GetUploadURL", tc, adapter, a.GetUploadURL)
			testGoCloudURLWithInvalidConfig(t, "GetGoCloudURL", tc, adapter, a.GetGoCloudURL)
			testUploadEnvWithInvalidConfig(t, "GetUploadEnv", tc, adapter, a.GetUploadEnv)
		})
	}
}

