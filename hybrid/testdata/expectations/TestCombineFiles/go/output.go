// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"fmt"
	"os"
)

func foo1() {
	// ruleid:go_file-permissions_rule-fileperm
	err := os.Chmod("/tmp/somefile", 0777)
	if err != nil {
		fmt.Println("Error when changing file permissions!")
		return
	}
}

func foo2() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Println("Error opening a file!")
		return
	}
}

func foo3() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Println("Error writing file")
		return
	}
}

func foo4() {
	// ok:go_file-permissions_rule-fileperm
	_, err := os.OpenFile("/tmp/thing", os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		fmt.Println("Error opening a file!")
		return
	}
}

func foo5() {
	// ok:go_file-permissions_rule-fileperm
	err := os.Chmod("/tmp/mydir", 0400)
	if err != nil {
		fmt.Println("Error")
		return
	}
}

func Factories() *FactoriesMap {
	return factories
}

func init() {
	_, _ = maxprocs.Set()
	memlimit.SetGoMemLimitWithEnv()
}

func prepareMockedCredentialsResolverInitializer(tc adapterOperationInvalidConfigTestCase) func() {
	oldCredentialsResolverInitializer := credentialsResolverInitializer
	credentialsResolverInitializer = func(config *common.CacheAzureConfig) (*defaultCredentialsResolver, error) {
		if tc.errorOnCredentialsResolverInitialization {
			return nil, errors.New("test error")
		}

		return newDefaultCredentialsResolver(config)
	}

	return func() {
		credentialsResolverInitializer = oldCredentialsResolverInitializer
	}
}

func defaultAzureCache() *common.CacheConfig {
	return &common.CacheConfig{
		Type: "azure",
		Azure: &common.CacheAzureConfig{
			CacheAzureCredentials: common.CacheAzureCredentials{
				AccountName: accountName,
				AccountKey:  accountKey,
			},
			ContainerName: containerName,
			StorageDomain: storageDomain,
		},
	}
}

func init() {
	err := cache.Factories().Register("azure", New)
	if err != nil {
		panic(err)
	}
}

func TestDoubledRegistration(t *testing.T) {
	adapterTypeName := "test"
	fakeFactory := func(config *common.CacheConfig, timeout time.Duration, objectName string) (Adapter, error) {
		return nil, nil
	}

	f := &FactoriesMap{}

	err := f.Register(adapterTypeName, fakeFactory)
	assert.NoError(t, err)
	assert.Len(t, f.internal, 1)

	err = f.Register(adapterTypeName, fakeFactory)
	assert.Error(t, err)
	assert.Len(t, f.internal, 1)
}

func prepareMockedCredentialsResolverForInvalidConfig(adapter *azureAdapter, tc adapterOperationInvalidConfigTestCase) {
	cr := &mockCredentialsResolver{}

	resolveCall := cr.On("Resolve")
	if tc.credentialsResolverResolveError {
		resolveCall.Return(fmt.Errorf("test error"))
	} else {
		resolveCall.Return(nil)
	}

	config := defaultAzureCache()
	config.Azure.CacheAzureCredentials.AccountName = tc.accountName
	config.Azure.CacheAzureCredentials.AccountKey = tc.accountKey
	config.Azure.ContainerName = tc.containerName

	// Always return an account key signer to avoid metadata lookups
	signer, err := newAccountKeySigner(config.Azure)
	cr.On("Signer").Return(signer, err)

	adapter.credentialsResolver = cr
}

func makeTestFactory(test factorizeTestCase) Factory {
	return func(config *common.CacheConfig, timeout time.Duration, objectName string) (Adapter, error) {
		if test.errorOnFactorize != nil {
			return nil, test.errorOnFactorize
		}

		return test.adapter, nil
	}
}

