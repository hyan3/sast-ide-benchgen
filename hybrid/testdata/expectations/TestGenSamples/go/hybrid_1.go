func TestCreateAdapter(t *testing.T) {
	adapterMock := new(MockAdapter)

	tests := map[string]factorizeTestCase{
		"adapter doesn't exist": {
			adapter:          nil,
			errorOnFactorize: nil,
			expectedAdapter:  nil,
			expectedError:    `cache factory not found: factory for cache adapter \"test\" was not registered`,
		},
		"adapter exists": {
			adapter:          adapterMock,
			errorOnFactorize: nil,
			expectedAdapter:  adapterMock,
			expectedError:    "",
		},
		"adapter errors on factorize": {
			adapter:          adapterMock,
			errorOnFactorize: errors.New("test error"),
			expectedAdapter:  nil,
			expectedError:    `cache adapter could not be initialized: test error`,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			cleanupFactoriesMap := prepareMockedFactoriesMap()
			defer cleanupFactoriesMap()

			adapterTypeName := "test"

			if test.adapter != nil {
				err := factories.Register(adapterTypeName, makeTestFactory(test))
				assert.NoError(t, err)
			}

			_ = factories.Register(
				"additional-adapter",
				func(config *common.CacheConfig, timeout time.Duration, objectName string) (Adapter, error) {
					return new(MockAdapter), nil
				})

			config := &common.CacheConfig{
				Type: adapterTypeName,
			}

			adapter, err := getCreateAdapter(config, defaultTimeout, "key")

			if test.expectedError == "" {
				assert.NoError(t, err)
			} else {
				assert.Error(t, err)
			}

			assert.Equal(t, test.expectedAdapter, adapter)
		})
	}
}

func TestCreateAdapter(t *testing.T) {
	adapterMock := new(MockAdapter)

	tests := map[string]factorizeTestCase{
		"adapter doesn't exist": {
			adapter:          nil,
			errorOnFactorize: nil,
			expectedAdapter:  nil,
			expectedError:    `cache factory not found: factory for cache adapter \"test\" was not registered`,
		},
		"adapter exists": {
			adapter:          adapterMock,
			errorOnFactorize: nil,
			expectedAdapter:  adapterMock,
			expectedError:    "",
		},
		"adapter errors on factorize": {
			adapter:          adapterMock,
			errorOnFactorize: errors.New("test error"),
			expectedAdapter:  nil,
			expectedError:    `cache adapter could not be initialized: test error`,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			cleanupFactoriesMap := prepareMockedFactoriesMap()
			defer cleanupFactoriesMap()

			adapterTypeName := "test"

			if test.adapter != nil {
				err := factories.Register(adapterTypeName, makeTestFactory(test))
				assert.NoError(t, err)
			}

			_ = factories.Register(
				"additional-adapter",
				func(config *common.CacheConfig, timeout time.Duration, objectName string) (Adapter, error) {
					return new(MockAdapter), nil
				})

			config := &common.CacheConfig{
				Type: adapterTypeName,
			}

			adapter, err := getCreateAdapter(config, defaultTimeout, "key")

			if test.expectedError == "" {
				assert.NoError(t, err)
			} else {
				assert.Error(t, err)
			}

			assert.Equal(t, test.expectedAdapter, adapter)
		})
	}
}


// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"fmt"
	"log"
	"net/http"
	_ "net/http/pprof"
)

func bad1() {
	http.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprintf(w, "Hello World!")
	})
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

func bad2() {
	http.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprintf(w, "Hello World!")
	})
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

func bad3() {
	http.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprintf(w, "Hello World!")
	})
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

func bad4() {
	http.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprintf(w, "Hello World!")
	})
	// ruleid:go_leak_rule-pprof-endpoint
	log.Fatal(http.ServeTLS(nil, nil, "cert.key", "key.enc"))
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"fmt"
	"log"
	"net/http"
	_ "net/http/pprof"
)

func bad1() {
	http.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprintf(w, "Hello World!")
	})
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

func bad2() {
	http.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprintf(w, "Hello World!")
	})
	// ruleid:go_leak_rule-pprof-endpoint
	log.Fatal(http.ListenAndServeTLS(":8443", "cert.crt", "key.enc", nil))
}

func bad3() {
	http.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprintf(w, "Hello World!")
	})
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

func bad4() {
	http.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprintf(w, "Hello World!")
	})
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}
