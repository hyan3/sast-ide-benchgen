module gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen

go 1.22.3

require (
	github.com/BurntSushi/toml v1.4.0
	github.com/dustin/go-humanize v1.0.1
	github.com/jarcoal/httpmock v1.3.1
	github.com/sirupsen/logrus v1.9.3
	github.com/smacker/go-tree-sitter v0.0.0-20240514083259-c5d1f3f5f99e
	github.com/stretchr/testify v1.8.0
	github.com/urfave/cli/v2 v2.27.4
	gitlab.com/gitlab-org/security-products/analyzers/common/v3 v3.4.0
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.4 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20240521201337-686a1a2994c1 // indirect
	golang.org/x/sys v0.22.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
