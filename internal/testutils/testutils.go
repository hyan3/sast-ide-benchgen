package testutils

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/config"
	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/helper"
)

// CompareDirs compares files in the top-level directories of expectedDir and actualDir.
// Returns true if files with the same names have identical contents, and there are no extra files in actualDir or expectedDir.
func CompareDirs(t *testing.T, expectedDir, actualDir string) {
	expectedFiles, err := filepath.Glob(filepath.Join(expectedDir, "*"))
	require.NoError(t, err)

	actualFiles, err := filepath.Glob(filepath.Join(actualDir, "*"))
	require.NoError(t, err)

	expectedFileMap := map[string]string{}
	for _, path := range expectedFiles {
		expectedFileMap[filepath.Base(path)] = path
	}

	for _, actualPath := range actualFiles {
		actualName := filepath.Base(actualPath)
		expectedPath, exists := expectedFileMap[actualName]

		// check if there are any additional files in the actual dir that are not present in the expected dir
		require.Truef(t, exists, "File %q exists in %q, but not in %q", actualName, actualDir, expectedDir)

		// both files exist, compare the contents
		if exists {
			require.Equalf(t, ReadFile(t, expectedPath), ReadFile(t, actualPath),
				"Contents of expected file %q do not match actual file %q", expectedPath, actualPath)
			delete(expectedFileMap, actualName) // Remove matched file from expectedFileMap
		}
	}

	// if there are any files left over in expectedFileMap, then there are additional files in the
	// expected dir that are not present in the actual dir
	for extraFile := range expectedFileMap {
		require.Failf(t, "Additional file detected", "File %q exists in %q, but not in %q", extraFile, expectedDir, actualDir)
	}
}

// ReadFile returns the contents of the filePath and checks for file read errors
func ReadFile(t *testing.T, filePath string) string {
	content, err := os.ReadFile(filePath)
	require.NoError(t, err)
	return string(content)
}

var fixedTmp = flag.Bool("test.fixed", false, "Use fixed 'tmp' directory instead of a temporary directory")

// TempDir creates a temporary directory for testing. Run tests with -test.fixed in order to use a fixed 'tmp'
// directory instead of a random one.
func TempDir(t *testing.T) string {
	flag.Parse()

	tempDir := "tmp"
	if *fixedTmp {
		require.NoError(t, os.MkdirAll(tempDir, os.ModePerm))
	} else {
		tempDir = t.TempDir()
	}

	t.Logf("Using tempdir %q", tempDir)
	return tempDir
}

// EqualFileCount counts the number of files matching glob and compares to the expectedNum
func EqualFileCount(t *testing.T, glob string, expectedNum int) {
	count, err := helper.FileCount(glob)
	require.NoError(t, err)
	require.Equal(t, expectedNum, count)
}

// EqualFileContent compares the contents of the file at expectedFilePath against actualContent
func EqualFileContent(t *testing.T, expectedFilePath, actualContent string) {
	expectedContent, err := os.ReadFile(expectedFilePath)
	require.NoError(t, err)
	require.Equal(t, string(expectedContent), actualContent, "Expectation file %q does not match actual content", expectedFilePath)
}

// EqualFiles compares the contents of the files at expectedFilePath and actualFilePath
func EqualFiles(t *testing.T, expectedFilePath, actualFilePath string) {
	expectedContent, err := os.ReadFile(expectedFilePath)
	require.NoError(t, err)
	actualContent, err := os.ReadFile(actualFilePath)
	require.NoError(t, err)
	require.Equal(t, string(expectedContent), string(actualContent), "Expectation file %q does not actual file %q",
		expectedFilePath, actualFilePath)
}

// FixturePath returns the fixture file path for the given language
func FixturePath(t *testing.T, lang string) string {
	langConfig, err := config.GetLanguageConfig(lang)
	require.NoError(t, err)

	sourceCodeFile := fmt.Sprintf("%s_code%s", lang, langConfig.Extension())
	return filepath.Join("../testdata/fixtures/shared", sourceCodeFile)
}
