package config

import (
	"fmt"
	"testing"

	"github.com/smacker/go-tree-sitter/golang"
	"github.com/smacker/go-tree-sitter/java"
	"github.com/smacker/go-tree-sitter/python"
	"github.com/smacker/go-tree-sitter/ruby"
	"github.com/stretchr/testify/require"
)

func TestGetLanguageConfig(t *testing.T) {
	tests := []struct {
		language string
		want     *TreeSitterConfig
		wantErr  error
	}{
		{
			language: "go",
			want: &TreeSitterConfig{
				extension:       ".go",
				language:        golang.GetLanguage(),
				commentSymbol:   "//",
				rawSeedQuery:    "(function_declaration) @function (method_declaration) @method",
				rawCommentQuery: "(comment) @comment",
			},
		},
		{
			language: "java",
			want: &TreeSitterConfig{
				extension:       ".java",
				language:        java.GetLanguage(),
				commentSymbol:   "//",
				rawSeedQuery:    "(class_declaration) @class",
				rawCommentQuery: "(line_comment) @comment",
			},
		},
		{
			language: "python",
			want: &TreeSitterConfig{
				extension:       ".py",
				language:        python.GetLanguage(),
				commentSymbol:   "#",
				rawSeedQuery:    "(class_definition) @class",
				rawCommentQuery: "(comment) @comment",
			},
		},
		{
			language: "ruby",
			want: &TreeSitterConfig{
				extension:       ".rb",
				language:        ruby.GetLanguage(),
				commentSymbol:   "#",
				rawSeedQuery:    "(class) @class",
				rawCommentQuery: "(comment) @comment",
			},
		},
		{
			language: "invalid",
			wantErr:  fmt.Errorf("unsupported language: invalid"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.language, func(t *testing.T) {
			got, err := GetLanguageConfig(tt.language)

			if tt.wantErr != nil {
				require.Equal(t, tt.wantErr, err)
			} else {
				require.NoError(t, err)
				require.Equal(t, tt.want, got)
			}
		})
	}
}

func TestCommentSymbol(t *testing.T) {
	languages := map[string]string{
		"go":     "//",
		"java":   "//",
		"python": "#",
		"ruby":   "#",
	}

	for lang, expected := range languages {
		t.Run(lang, func(t *testing.T) {
			langConfig, err := GetLanguageConfig(lang)
			require.NoError(t, err)

			result := langConfig.CommentSymbol()
			require.Equal(t, expected, result)
		})
	}
}

func TestSeedQuery(t *testing.T) {
	tests := []string{
		"go",
		"java",
		"python",
		"ruby",
	}

	for _, lang := range tests {
		t.Run(lang, func(t *testing.T) {
			langConfig, err := GetLanguageConfig(lang)
			require.NoError(t, err)

			query, err := langConfig.SeedQuery()
			require.NoError(t, err)
			require.NotNil(t, query)
		})
	}
}

func TestCommentQuery(t *testing.T) {
	tests := []string{
		"go",
		"java",
		"python",
		"ruby",
	}

	for _, lang := range tests {
		t.Run(lang, func(t *testing.T) {
			langConfig, err := GetLanguageConfig(lang)
			require.NoError(t, err)

			query, err := langConfig.CommentQuery()
			require.NoError(t, err)
			require.NotNil(t, query)
		})
	}
}
