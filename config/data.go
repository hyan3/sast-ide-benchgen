package config

// ProgramData holds the configuration for number of vulnerabilities and lines of code to generate.
type ProgramData struct {
	NumVulnerabilities, LinesOfCodeToGenerate int
}

// Configurations defines preset configurations based on language, program size, and vulnerability level.
var Configurations = map[string]map[string]map[string]ProgramData{
	"python": {
		"small": {
			"low":  ProgramData{NumVulnerabilities: 1, LinesOfCodeToGenerate: 550},
			"high": ProgramData{NumVulnerabilities: 5, LinesOfCodeToGenerate: 550},
		},
		"large": {
			"low":  ProgramData{NumVulnerabilities: 2, LinesOfCodeToGenerate: 2000},
			"high": ProgramData{NumVulnerabilities: 18, LinesOfCodeToGenerate: 2000},
		},
	},
	"java": {
		"small": {
			"low":  ProgramData{NumVulnerabilities: 1, LinesOfCodeToGenerate: 700},
			"high": ProgramData{NumVulnerabilities: 7, LinesOfCodeToGenerate: 500},
		},
		"large": {
			"low":  ProgramData{NumVulnerabilities: 5, LinesOfCodeToGenerate: 2500},
			"high": ProgramData{NumVulnerabilities: 25, LinesOfCodeToGenerate: 2500},
		},
	},
	"go": {
		"small": {
			"low":  ProgramData{NumVulnerabilities: 1, LinesOfCodeToGenerate: 500},
			"high": ProgramData{NumVulnerabilities: 4, LinesOfCodeToGenerate: 500},
		},
		"large": {
			"low":  ProgramData{NumVulnerabilities: 2, LinesOfCodeToGenerate: 1500},
			"high": ProgramData{NumVulnerabilities: 11, LinesOfCodeToGenerate: 1500},
		},
	},
	"ruby": {
		"small": {
			"low":  ProgramData{NumVulnerabilities: 1, LinesOfCodeToGenerate: 600},
			"high": ProgramData{NumVulnerabilities: 5, LinesOfCodeToGenerate: 600},
		},
		"large": {
			"low":  ProgramData{NumVulnerabilities: 4, LinesOfCodeToGenerate: 2200},
			"high": ProgramData{NumVulnerabilities: 17, LinesOfCodeToGenerate: 2200},
		},
	},
}
