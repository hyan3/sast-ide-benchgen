package config

import (
	"fmt"

	sitter "github.com/smacker/go-tree-sitter"
	"github.com/smacker/go-tree-sitter/golang"
	"github.com/smacker/go-tree-sitter/java"
	"github.com/smacker/go-tree-sitter/python"
	"github.com/smacker/go-tree-sitter/ruby"
)

// LanguageConfig is an interface for implementing configurations for languages
type LanguageConfig interface {
	Language() *sitter.Language
	Extension() string
	CommentSymbol() string
	CommentQuery() (*sitter.Query, error)
	SeedQuery() (*sitter.Query, error)
}

// TreeSitterConfig defines the language and query for Tree-sitter parsing.
type TreeSitterConfig struct {
	extension       string
	language        *sitter.Language
	commentSymbol   string
	rawSeedQuery    string
	rawCommentQuery string
	seedQuery       *sitter.Query
	commentQuery    *sitter.Query
}

// treeSitterConfigs holds the configuration for different programming languages supported.
var treeSitterConfigs = map[string]*TreeSitterConfig{
	"go": {
		extension:       ".go",
		language:        golang.GetLanguage(),
		commentSymbol:   "//",
		rawSeedQuery:    `(function_declaration) @function (method_declaration) @method`,
		rawCommentQuery: `(comment) @comment`,
	},
	"java": {
		extension:       ".java",
		language:        java.GetLanguage(),
		commentSymbol:   "//",
		rawSeedQuery:    `(class_declaration) @class`,
		rawCommentQuery: `(line_comment) @comment`,
	},
	"python": {
		extension:       ".py",
		language:        python.GetLanguage(),
		commentSymbol:   "#",
		rawSeedQuery:    `(class_definition) @class`,
		rawCommentQuery: `(comment) @comment`,
	},
	"ruby": {
		extension:       ".rb",
		language:        ruby.GetLanguage(),
		commentSymbol:   "#",
		rawSeedQuery:    `(class) @class`,
		rawCommentQuery: `(comment) @comment`,
	},
}

// GetLanguageConfig fetches language-specific configuration
func GetLanguageConfig(lang string) (LanguageConfig, error) {
	config, exists := treeSitterConfigs[lang]
	if !exists {
		return nil, fmt.Errorf("unsupported language: %s", lang)
	}
	return config, nil
}

// SeedQuery returns a Tree-sitter query instance for extracting seed elements.
func (t *TreeSitterConfig) SeedQuery() (*sitter.Query, error) {
	if t.seedQuery == nil {
		query, err := sitter.NewQuery([]byte(t.rawSeedQuery), t.language)
		if err != nil {
			return nil, fmt.Errorf("failed to create seed query: %v", err)
		}
		t.seedQuery = query
	}
	return t.seedQuery, nil
}

// CommentQuery returns a Tree-sitter query instance for extracting comments.
func (t *TreeSitterConfig) CommentQuery() (*sitter.Query, error) {
	if t.commentQuery == nil {
		query, err := sitter.NewQuery([]byte(t.rawCommentQuery), t.language)
		if err != nil {
			return nil, fmt.Errorf("failed to create comment query: %v", err)
		}
		t.commentQuery = query
	}
	return t.commentQuery, nil
}

// Extension returns the file extension for the language
func (t *TreeSitterConfig) Extension() string {
	return t.extension
}

// Language returns the Tree-sitter language
func (t *TreeSitterConfig) Language() *sitter.Language {
	return t.language
}

// CommentSymbol returns the comment symbol for the language
func (t *TreeSitterConfig) CommentSymbol() string {
	return t.commentSymbol
}
