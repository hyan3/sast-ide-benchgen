package config

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestLoad(t *testing.T) {
	got, err := Load("testdata/fixtures/TestLoad/config.toml")
	require.NoError(t, err)

	want := &AppConfig{
		BaselineRepoURL:            "https://gitlab.com/gitlab-org/security-products/sast-rules.git",
		BaselineSamplesDir:         "out/baseline",
		CustomSeedsDir:             "tmp/seeds_custom",
		GeneratedHybridSamplesDir:  "out/hybrid",
		GeneratedOssSamplesDir:     "out/oss",
		GitLabAPIURL:               "https://gitlab.example.com",
		Lang:                       "go",
		LinesOfCodeToGenerate:      500,
		NumOssSamplesToGenerate:    500,
		NumHybridSamplesToGenerate: 500,
		NumVulnerabilities:         4,
		OutDir:                     "./out",
		RandSeed:                   123,
		SeedFuncsDir:               "tmp/seeds",
		SeedsMax:                   2000,
		SeedTreeHeightMax:          2147483647,
		SeedTreeHeightMin:          20,
		TmpDir:                     "./tmp",
		TopOssDir:                  "tmp/top_oss"}

	require.Equal(t, want, got)
}
