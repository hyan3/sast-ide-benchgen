package config

import (
	"fmt"
	"path/filepath"
	"regexp"
	"time"

	"github.com/BurntSushi/toml"
)

// AppConfig contains parameters loaded from config.toml.
type AppConfig struct {
	// BenchGenConfig contains parameters loaded from config.toml.
	// see https://gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/blob/main/config.toml
	// for an explanation of what each field represents.
	BaselineRepoURL            string
	BaselineSamplesDir         string // path within OutDir for the baseline samples.
	CustomSeedsDir             string // path within TmpDir for temporarily storing the custom seed functions
	GeneratedHybridSamplesDir  string // path within OutDir for the generated hybrid samples.
	GeneratedOssSamplesDir     string // path within OutDir for the generated OSS samples.
	GitLabAPIURL               string
	Lang                       string
	LinesOfCodeToGenerate      int
	NumHybridSamplesToGenerate int
	NumOssSamplesToGenerate    int
	NumVulnerabilities         int
	OutDir                     string
	RandSeed                   int64  // allows us to hardcode a seed value to make tests deterministic
	SeedFuncsDir               string // path within TmpDir for temporarily storing the seed functions
	SeedsMax                   int
	SeedTreeHeightMax          int
	SeedTreeHeightMin          int
	TmpDir                     string
	TopOssDir                  string // path within TmpDir for temporarily storing the top OSS projects
}

// Default configuration constants for directories used in the project.
const (
	BaselineSamplesDir        = "./baseline"     // relative path to the baseline samples
	CustomSeedsDir            = "./seeds_custom" // relative path to the temporary custom seed functions
	GeneratedHybridSamplesDir = "./hybrid"       // relative path to the generated hybrid samples
	GeneratedOssSamplesDir    = "./oss"          // relative path to the generated OSS samples.
	SeedFuncsDir              = "./seeds"        // relative path to the temporary seed functions
	TopOssDir                 = "./top_oss"      // relative path to the pulled top oss projects
)

// Load reads the application configuration from the specified TOML file,
// initializes the Tree-sitter settings based on the language, and returns the AppConfig.
func Load(path string) (*AppConfig, error) {
	config := AppConfig{}
	if _, err := toml.DecodeFile(path, &config); err != nil {
		return nil, fmt.Errorf("failed to decode toml: %v", err)
	}

	if config.RandSeed == 0 {
		config.RandSeed = time.Now().UnixNano()
	}

	config.TopOssDir = filepath.Join(config.TmpDir, TopOssDir)
	config.SeedFuncsDir = filepath.Join(config.TmpDir, SeedFuncsDir)
	config.CustomSeedsDir = filepath.Join(config.TmpDir, CustomSeedsDir)
	config.GeneratedOssSamplesDir = filepath.Join(config.OutDir, GeneratedOssSamplesDir)
	config.BaselineSamplesDir = filepath.Join(config.OutDir, BaselineSamplesDir)
	config.GeneratedHybridSamplesDir = filepath.Join(config.OutDir, GeneratedHybridSamplesDir)

	return &config, nil
}

// RuleidRegex matches comment lines that identify vulnerabilities in `semgrep` testing, following a Semgrep convention.
var RuleidRegex = regexp.MustCompile(`(?m)^\s*(//|#)\s*ruleid:`)

// SastRulesPlaceholderRegex matches a placeholder for SAST rules.
var SastRulesPlaceholderRegex = regexp.MustCompile(`TODO: Placeholder`)
