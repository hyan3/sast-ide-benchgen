image: alpine:latest

stages:
  - test
  - build-img
  - reuse-oss-samples
  - synthesize-samples

variables:
  DOCKER_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG

include:
  - https://gitlab.com/gitlab-org/security-products/ci-templates/raw/master/includes-dev/go.yml

integration_test:
  extends: .go
  script:
    - go test --tags=integration ./...

build:
  image: docker:26
  stage: build-img
  services:
    - docker:26-dind
  script:
    - docker info
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker buildx build --output="type=docker,name=$DOCKER_IMAGE,compression=gzip,compression-level=9,force-compression=true" .
    - docker push $DOCKER_IMAGE

generate oss samples:
  stage: reuse-oss-samples
  image: $DOCKER_IMAGE
  script:
    - mv /root/benchgen .
    - ./benchgen oss
  artifacts:
    paths:
      - out/oss/

pull baseline samples:
  stage: reuse-oss-samples
  image: $DOCKER_IMAGE
  script:
    - mv /root/benchgen .
    - ./benchgen baseline
  artifacts:
    paths:
      - out/baseline/

generate hybrid samples:
  stage: synthesize-samples
  image: $DOCKER_IMAGE
  dependencies:
    - generate oss samples
    - pull baseline samples
  script:
    - mv /root/benchgen .
    - ./benchgen hybrid
  artifacts:
    paths:
      - out/hybrid/

goimports:
  extends: .go
  script:
    - go install golang.org/x/tools/cmd/goimports@latest
    # the testdata dirs contain incorrectly formatted go code in the fixtures
    # and expectations dirs so we need to ignore them before running goimports,
    # otherwise the goimports job will fail
    - FMT_CMD="find . -type f -name '*.go' ! -path '*/testdata/*' -exec goimports -w -local gitlab.com/gitlab-org {} +"
    - eval "$FMT_CMD"
    - |
      if ! git diff --ignore-submodules=dirty --exit-code; then
        echo
        echo "Some files are not formatted. Please format with \`$FMT_CMD\`"
        echo 'See https://docs.gitlab.com/ee/development/go_guide/#code-style-and-format-1 for more information'
        exit 1
      fi
