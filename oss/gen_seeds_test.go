package oss

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/config"
	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/internal/testutils"
)

func TestGenerateSeeds(t *testing.T) {
	t.Run("No existing files in SeedFuncsDir", func(t *testing.T) {
		tempDir := testutils.TempDir(t)
		expectationsDir := "testdata/expectations/TestGenerateSeeds/go/default"

		appConfig := &config.AppConfig{
			SeedFuncsDir: tempDir,
			TopOssDir:    "testdata/fixtures/TestGenerateSeeds/go",
			Lang:         "go",
			SeedsMax:     5,
		}

		err := generateSeeds(appConfig)
		require.NoError(t, err)

		testutils.CompareDirs(t, expectationsDir, tempDir)
	})

	t.Run("Execute generateSeeds twice with the same parameters", func(t *testing.T) {
		tempDir := testutils.TempDir(t)
		expectationsDir := "testdata/expectations/TestGenerateSeeds/go/default"

		appConfig := &config.AppConfig{
			SeedFuncsDir: tempDir,
			TopOssDir:    "testdata/fixtures/TestGenerateSeeds/go",
			Lang:         "go",
			SeedsMax:     5,
		}

		err := generateSeeds(appConfig)
		require.NoError(t, err)

		testutils.CompareDirs(t, expectationsDir, tempDir)

		err = generateSeeds(appConfig)
		require.NoError(t, err)

		// result doesn't change, even though we've executed generateSeeds twice
		testutils.CompareDirs(t, expectationsDir, tempDir)
	})

	// test when generateSeeds is called on a directory that already contains seed files,
	// but the existing seed files don't match the files from the configured TopOssDir
	t.Run("Non-matching existing files in SeedFuncsDir", func(t *testing.T) {
		tempDir := testutils.TempDir(t)
		expectationsDir := "testdata/expectations/TestGenerateSeeds/go/non-matching-existing-seed-files"

		appConfig := &config.AppConfig{
			SeedFuncsDir: tempDir,
			TopOssDir:    "testdata/fixtures/TestGenerateSeeds/go",
			Lang:         "go",
			SeedsMax:     5,
		}

		files := []string{"123.seed", "456.seed", "789.seed"}
		for _, file := range files {
			filePath := filepath.Join(tempDir, file)
			file, err := os.Create(filePath)
			require.NoError(t, err)
			file.Close()
		}

		err := generateSeeds(appConfig)
		require.NoError(t, err)

		testutils.CompareDirs(t, expectationsDir, tempDir)

		expectedFileCount := appConfig.SeedsMax + len(files)
		testutils.EqualFileCount(t, filepath.Join(expectationsDir, "*.seed"), expectedFileCount)
	})
}
