package oss

import (
	"testing"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/config"
	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/internal/testutils"

	"github.com/stretchr/testify/require"
)

func TestConcatSeeds(t *testing.T) {
	fixturesDir := "testdata/fixtures/TestConcatSeeds"
	expectationsDir := "testdata/expectations/TestConcatSeeds"
	outputDir := testutils.TempDir(t)

	appConfig := config.AppConfig{
		CustomSeedsDir:          fixturesDir,
		GeneratedOssSamplesDir:  outputDir,
		NumOssSamplesToGenerate: 2,
		LinesOfCodeToGenerate:   100,
		Lang:                    "ruby",
		RandSeed:                1, // hardcode the seed value for deterministic results
	}

	err := concatSeeds(&appConfig)
	require.NoError(t, err)
	testutils.CompareDirs(t, expectationsDir, outputDir)
}

func TestCountLinesInFile(t *testing.T) {
	want := 157

	got, err := countLinesInFile(testutils.FixturePath(t, "python"))
	require.NoError(t, err)
	require.Equal(t, want, got)
}
