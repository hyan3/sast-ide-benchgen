package oss

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/config"
)

// Generate executes the full process of pulling repositories, generating seeds,
// customizing seeds, and generating sample files based on the configuration.
func Generate(appConfig *config.AppConfig) error {
	log.Info("Step 1: Fetching repos ...")
	if err := fetchTopRepos(appConfig); err != nil {
		return err
	}

	log.Info("Step 2: Generating seeds ...")
	if err := generateSeeds(appConfig); err != nil {
		return err
	}

	log.Info("Step3: Customising seeds ...")
	if err := customiseSeeds(appConfig); err != nil {
		return err
	}

	log.Info("Step 4: Generating Samples ...")
	if err := concatSeeds(appConfig); err != nil {
		return err
	}

	return nil
}
