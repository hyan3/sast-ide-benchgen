package oss

import (
	"context"
	"fmt"
	"math/rand"
	"path/filepath"

	sitter "github.com/smacker/go-tree-sitter"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/config"
	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/internal/testutils"

	"testing"

	"github.com/stretchr/testify/require"
)

func TestCalculateTreeHeight(t *testing.T) {
	tests := []struct {
		sourceCodeFile string
		lang           string
		want           int
	}{
		{
			lang: "java",
			want: 12,
		},
		{
			lang: "ruby",
			want: 10,
		},
		{
			lang: "go",
			want: 9,
		},
		{
			lang: "python",
			want: 12,
		},
	}

	for _, tt := range tests {
		t.Run(tt.lang, func(t *testing.T) {
			fixturePath := testutils.FixturePath(t, tt.lang)

			sourceCode := testutils.ReadFile(t, fixturePath)
			parser := sitter.NewParser()
			langConfig, err := config.GetLanguageConfig(tt.lang)
			require.NoError(t, err)
			parser.SetLanguage(langConfig.Language())
			tree, err := parser.ParseCtx(context.Background(), nil, []byte(sourceCode))
			require.NoError(t, err)
			treeHeight := calculateTreeHeight(tree.RootNode())
			require.Equal(t, tt.want, treeHeight)
		})
	}
}

func TestCustomiseSeeds(t *testing.T) {
	fixturesDir := "testdata/fixtures/TestCustomiseSeeds"
	expectationsDir := "testdata/expectations/TestCustomiseSeeds"

	outputDir := testutils.TempDir(t)
	t.Logf("Using tempdir %q", outputDir)

	appConfig := config.AppConfig{
		SeedFuncsDir:      fixturesDir,
		CustomSeedsDir:    outputDir,
		SeedTreeHeightMin: 2,
		SeedTreeHeightMax: 9,
		Lang:              "go",
	}

	err := customiseSeeds(&appConfig)
	require.NoError(t, err)
	testutils.CompareDirs(t, expectationsDir, outputDir)
}

func TestCreateFileFromSeeds(t *testing.T) {
	tests := []struct {
		name, scenario, lang  string
		linesOfCodeToGenerate int
		noErr                 bool
	}{
		{
			name:                  "TestCreateFileFromSeeds",
			scenario:              "default",
			lang:                  "java",
			linesOfCodeToGenerate: 30,
			noErr:                 true,
		},
		{
			name:                  "TestCreateFileFromSeeds",
			scenario:              "err",
			lang:                  "java",
			linesOfCodeToGenerate: 100, // Impossible to generate 100 lines of code using a seed with 500 lines of code
			noErr:                 false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.scenario, func(t *testing.T) {
			expectDir := filepath.Join("testdata/expectations/", tt.name, tt.scenario)
			fixturesDir := filepath.Join("testdata/fixtures/", tt.name, tt.scenario)
			seedFiles, err := filepath.Glob(filepath.Join(fixturesDir, "*"))
			require.NoError(t, err)
			langConfig, err := config.GetLanguageConfig(tt.lang)
			outputDir := testutils.TempDir(t)
			outputFilePath := filepath.Join(outputDir, fmt.Sprintf("output_%d%s", 0, langConfig.Extension()))
			r := rand.New(rand.NewSource(1))
			err = createFileFromSeeds(seedFiles, outputFilePath, tt.linesOfCodeToGenerate, r)
			if tt.noErr {
				require.NoError(t, err)
				testutils.CompareDirs(t, expectDir, outputDir)
			} else {
				require.Error(t, err)
			}
		})
	}
}
