package oss

import (
	"testing"

	"github.com/jarcoal/httpmock"
	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/helper"
)

func TestSearchForTopRepos(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	mockURL := "some-url"

	tests := []struct {
		name, body string
		status     int
		wantErr    bool
		want       []helper.Repo
	}{
		{
			name:   "ok",
			status: 200,
			body: `[{"name": "repo1", "name_with_namespace": "user/repo1", "http_url_to_repo": "https://github.com/user/repo1"},
              {"name": "repo2", "name_with_namespace": "user/repo2", "http_url_to_repo": "https://github.com/user/repo2"}]`,
			want: []helper.Repo{
				{Name: "repo1", FullName: "user/repo1", HTMLURL: "https://github.com/user/repo1"},
				{Name: "repo2", FullName: "user/repo2", HTMLURL: "https://github.com/user/repo2"},
			},
		},
		{
			name:    "invalid response",
			status:  200,
			body:    "invalid-json",
			wantErr: true,
		},
		{
			name:    "not found",
			status:  404,
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			responder := httpmock.NewStringResponder(tt.status, tt.body)
			httpmock.RegisterResponder("GET", mockURL, responder)
			got, err := searchForTopRepos(mockURL)

			if tt.wantErr {
				require.Error(t, err)
			} else {
				require.NoError(t, err)
				require.Equal(t, tt.want, got)
			}
		})
	}
}
