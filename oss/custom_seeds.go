package oss

import (
	"context"
	"fmt"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	sitter "github.com/smacker/go-tree-sitter"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/config"
	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/helper"
)

// customiseSeeds filters and copies seed files based on tree height criteria,
// saving the results in the configured custom seeds directory.
func customiseSeeds(appConfig *config.AppConfig) error {
	// create CustomSeeds
	if err := os.MkdirAll(appConfig.CustomSeedsDir, os.ModePerm); err != nil {
		return fmt.Errorf("failed to create %s directory: %v", appConfig.CustomSeedsDir, err)
	}
	seedFiles, err := helper.GetFilesByExtension(appConfig.SeedFuncsDir, ".seed")
	if err != nil {
		return fmt.Errorf("failed to get seed files with extension %q: %v", ".seed", err)
	}

	langConfig, err := config.GetLanguageConfig(appConfig.Lang)
	if err != nil {
		return err
	}

	parser := sitter.NewParser()
	parser.SetLanguage(langConfig.Language())

	customisedSeedCount := 0
	for _, file := range seedFiles {
		content, err := os.ReadFile(file)
		if err != nil {
			return fmt.Errorf("failed to read file %s: %v", file, err)
		}

		tree, err := parser.ParseCtx(context.Background(), nil, content)
		if err != nil {
			return fmt.Errorf("error occured when parsing with treesitter: %v", err)
		}

		treeHeight := calculateTreeHeight(tree.RootNode())

		if treeHeight >= appConfig.SeedTreeHeightMin && treeHeight <= appConfig.SeedTreeHeightMax {
			customisedSeedCount++
			log.Debugf("%d tree height = %d", customisedSeedCount, treeHeight)
			_, fileName := filepath.Split(file)
			dstFilePath := filepath.Join(appConfig.CustomSeedsDir, fileName)
			if err := helper.CopyFile(file, dstFilePath); err != nil {
				return fmt.Errorf("failed to copy file %s to %s: %v", file, dstFilePath, err)
			}
		}
	}

	log.Infof("Filtered files have been copied to %s directory.", appConfig.CustomSeedsDir)
	return nil
}

// calculateTreeHeight compute tree height
func calculateTreeHeight(node *sitter.Node) int {
	if node == nil {
		return 0
	}

	maxHeight := 0
	for i := 0; i < int(node.ChildCount()); i++ {
		child := node.Child(i)
		childHeight := calculateTreeHeight(child)
		if childHeight > maxHeight {
			maxHeight = childHeight
		}
	}
	return maxHeight + 1
}
