package oss

import (
	"bufio"
	"fmt"
	"io"
	"math/rand"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/config"
)

// concatSeeds generates sample files by combining seed files and saves them to the configured directory.
func concatSeeds(appConfig *config.AppConfig) error {
	// create output dir
	if err := os.MkdirAll(appConfig.GeneratedOssSamplesDir, os.ModePerm); err != nil {
		return fmt.Errorf("failed to create output directory: %v", err)
	}

	// read seed files
	seedFiles, err := filepath.Glob(filepath.Join(appConfig.CustomSeedsDir, "*"))
	if err != nil {
		return fmt.Errorf("failed to read seed files: %v", err)
	}

	if len(seedFiles) == 0 {
		return fmt.Errorf("no seed files found")
	}

	langConfig, err := config.GetLanguageConfig(appConfig.Lang)
	if err != nil {
		return err
	}

	r := rand.New(rand.NewSource(appConfig.RandSeed))

	// generate a specified number of sample files
	for i := 0; i < appConfig.NumOssSamplesToGenerate; i++ {
		outputFilePath := filepath.Join(appConfig.GeneratedOssSamplesDir, fmt.Sprintf("output_%d%s", i, langConfig.Extension()))

		if err := createFileFromSeeds(seedFiles, outputFilePath, appConfig.LinesOfCodeToGenerate, r); err != nil {
			return err
		}

		// output the info of the file generated
		fileInfo, err := os.Stat(outputFilePath)
		if err != nil {
			return fmt.Errorf("failed to get file info: %v", err)
		}

		log.Infof("Generated file: %s (%d bytes)", outputFilePath, fileInfo.Size())
	}

	log.Info("File generation completed.")
	return nil
}

const marginOfErr = 300 // MoE for generated lines of code, as it cannot be 100% precise
const maxAttempts = 100

func createFileFromSeeds(seedFiles []string, outputFile string, maxLines int, r *rand.Rand) error {
	outFile, err := os.Create(outputFile)
	if err != nil {
		return fmt.Errorf("failed to create output file: %v", err)
	}
	defer outFile.Close()

	for currentLines, currentAttempts := 0, 0; currentLines < maxLines; currentAttempts++ {
		if currentAttempts > maxAttempts {
			return fmt.Errorf("failed to create file from seeds: %v", err)
		}
		// randomly pick a seed
		seedFile := seedFiles[r.Intn(len(seedFiles))]
		linesInSeedFile, err := countLinesInFile(seedFile)
		if err != nil {
			return fmt.Errorf("failed to count lines in seed file: %v", err)
		}

		// discard if exceeds maxSize after adding the seed
		if currentLines+linesInSeedFile > maxLines+marginOfErr {
			continue
		}

		// add seed content
		if err := appendFileContent(seedFile, outFile); err != nil {
			return err
		}

		// append line break
		if _, err = outFile.WriteString("\n\n"); err != nil {
			return fmt.Errorf("failed to write newline to output file: %v", err)
		}

		// add 2 because we inserted two line breaks
		currentLines += linesInSeedFile + 2
	}

	return nil
}

func countLinesInFile(filePath string) (int, error) {
	file, err := os.Open(filePath)
	if err != nil {
		return 0, fmt.Errorf("failed to open file: %v", err)
	}
	defer file.Close()

	lineCount := 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lineCount++
	}
	if err := scanner.Err(); err != nil {
		return 0, fmt.Errorf("error reading file: %v", err)
	}

	return lineCount, nil
}

func appendFileContent(seedFile string, outFile *os.File) error {
	inFile, err := os.Open(seedFile)
	if err != nil {
		return fmt.Errorf("failed to open seed file: %v", err)
	}
	defer inFile.Close()

	if _, err = io.Copy(outFile, inFile); err != nil {
		return fmt.Errorf("failed to write to output file: %v", err)
	}

	return nil
}
