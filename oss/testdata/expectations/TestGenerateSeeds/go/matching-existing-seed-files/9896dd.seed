func (c *VerifyCommand) Execute(context *cli.Context) {
	userModeWarning(true)

	err := c.loadConfig()
	if err != nil {
		logrus.Fatalln(err)
		return
	}

	// check if there's something to verify
	toVerify, okRunners, err := c.selectRunners()
	if err != nil {
		logrus.Fatalln(err)
		return
	}

	// verify if runner exist
	for _, runner := range toVerify {
		if c.network.VerifyRunner(runner.RunnerCredentials, runner.SystemIDState.GetSystemID()) != nil {
			okRunners = append(okRunners, runner)
		}
	}

	// check if anything changed
	if len(c.getConfig().Runners) == len(okRunners) {
		return
	}

	if !c.DeleteNonExisting {
		logrus.Fatalln("Failed to verify runners")
		return
	}

	c.configMutex.Lock()
	c.config.Runners = okRunners
	c.configMutex.Unlock()

	// save config file
	err = c.saveConfig()
	if err != nil {
		logrus.Fatalln("Failed to update", c.ConfigFile, err)
	}
	logrus.Println("Updated", c.ConfigFile)
}