import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class LibrarySystem {
    private Map<String, Book> books;
    private Map<String, User> users;
    private List<Loan> loans;

    public LibrarySystem() {
        books = new HashMap<>();
        users = new HashMap<>();
        loans = new ArrayList<>();
    }

    public void addBook(String id, String title, String author, int copies) {
        books.put(id, new Book(id, title, author, copies));
    }

    public void removeBook(String id) {
        books.remove(id);
    }

    public Book findBook(String id) {
        return books.get(id);
    }

    public void displayBooks() {
        System.out.println("Library Books:");
        for (Book book : books.values()) {
            System.out.println(book);
        }
    }

    public void addUser(String userId, String name) {
        users.put(userId, new User(userId, name));
    }

    public void removeUser(String userId) {
        users.remove(userId);
    }

    public User findUser(String userId) {
        return users.get(userId);
    }

    public void displayUsers() {
        System.out.println("Library Users:");
        for (User user : users.values()) {
            System.out.println(user);
        }
    }

    public boolean borrowBook(String userId, String bookId) {
        User user = findUser(userId);
        Book book = findBook(bookId);
        if (user != null && book != null && book.getAvailableCopies() > 0) {
            book.borrow();
            Loan loan = new Loan(userId, bookId);
            loans.add(loan);
            user.addLoan(loan);
            return true;
        }
        return false;
    }

    public boolean returnBook(String userId, String bookId) {
        User user = findUser(userId);
        Book book = findBook(bookId);
        if (user != null && book != null) {
            for (Loan loan : loans) {
                if (loan.getUserId().equals(userId) && loan.getBookId().equals(bookId)) {
                    loans.remove(loan);
                    book.returnBook();
                    user.removeLoan(loan);
                    return true;
                }
            }
        }
        return false;
    }

    public void displayLoans() {
        System.out.println("Library Loans:");
        for (Loan loan : loans) {
            System.out.println("User: " + loan.getUserId() + " Book: " + loan.getBookId());
        }
    }

    private static class Book {
        private String id;
        private String title;
        private String author;
        private int copies;
        private int borrowedCopies;

        public Book(String id, String title, String author, int copies) {
            this.id = id;
            this.title = title;
            this.author = author;
            this.copies = copies;
            this.borrowedCopies = 0;
        }
        public String getId() {return id;}
        public String getTitle() {return title;}
        public String getAuthor() {return author;}
        public int getAvailableCopies() {return copies - borrowedCopies;}

        public void borrow() {
            if (borrowedCopies < copies) {
                borrowedCopies++;
            }
        }

        public void returnBook() {
            if (borrowedCopies > 0) {
                borrowedCopies--;
            }
        }

        @Override
        public String toString() {
            return "Book{" + "id='" + id + '\'' + ", title='" + title + '\'' + ", author='" + author + '\'' + ", availableCopies=" + getAvailableCopies() + '}';
        }
    }

    private static class User {
        private String id;
        private String name;
        private List<Loan> loans;

        public User(String id, String name) {
            this.id = id;
            this.name = name;
            this.loans = new ArrayList<>();
        }

        public String getId() {
            return id;
        }

        public String getName() {
            return name;
        }

        public void addLoan(Loan loan) {
            loans.add(loan);
        }

        public void removeLoan(Loan loan) {
            loans.remove(loan);
        }

        public List<Loan> getLoans() {
            return loans;
        }

        @Override
        public String toString() {
            return "User{" + "id='" + id + '\'' + ", name='" + name + '\'' + ", loans=" + loans.size() + '}';
        }
    }

    private static class Loan {
        private String userId;
        private String bookId;

        public Loan(String userId, String bookId) {
            this.userId = userId;
            this.bookId = bookId;
        }

        public String getUserId() {
            return userId;
        }

        public String getBookId() {
            return bookId;
        }
    }

    public static void main(String[] args) {
        LibrarySystem library = new LibrarySystem();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Library System");
            System.out.println("1. Add Book");
            System.out.println("2. Remove Book");
            System.out.println("3. Display Books");
            System.out.println("4. Add User");
            System.out.println("5. Remove User");
            System.out.println("6. Display Users");
            System.out.println("7. Borrow Book");
            System.out.println("8. Return Book");
            System.out.println("9. Display Loans");
            System.out.println("10. Exit");
            System.out.print("Enter choice: ");
            int choice = scanner.nextInt();
            scanner.nextLine();
            if (choice == 1) {
                System.out.print("Enter book ID: ");
                String id = scanner.nextLine();
                System.out.print("Enter book title: ");
                String title = scanner.nextLine();
                System.out.print("Enter book author: ");
                String author = scanner.nextLine();
                System.out.print("Enter number of copies: ");
                int copies = scanner.nextInt();
                library.addBook(id, title, author, copies);
            } else if (choice == 2) {
                System.out.print("Enter book ID: ");
                String id = scanner.nextLine();
                library.removeBook(id);
            } else if (choice == 3) {
                library.displayBooks();
            } else if (choice == 4) {
                System.out.print("Enter user ID: ");
                String userId = scanner.nextLine();
                System.out.print("Enter user name: ");
                String name = scanner.nextLine();
                library.addUser(userId, name);
            } else if (choice == 5) {
                System.out.print("Enter user ID: ");
                String userId = scanner.nextLine();
                library.removeUser(userId);
            } else if (choice == 6) {
                library.displayUsers();
            } else if (choice == 7) {
                System.out.print("Enter user ID: ");
                String userId = scanner.nextLine();
                System.out.print("Enter book ID: ");
                String bookId = scanner.nextLine();
                if (library.borrowBook(userId, bookId)) {
                    System.out.println("Book borrowed successfully.");
                } else {
                    System.out.println("Book could not be borrowed.");
                }
            } else if (choice == 8) {
                System.out.print("Enter user ID: ");
                String userId = scanner.nextLine();
                System.out.print("Enter book ID: ");
                String bookId = scanner.nextLine();
                if (library.returnBook(userId, bookId)) {
                    System.out.println("Book returned successfully.");
                } else {
                    System.out.println("Book could not be returned.");
                }
            } else if (choice == 9) {
                library.displayLoans();
            } else if (choice == 10) {
                System.out.println("Exiting...");
                break;
            } else {
                System.out.println("Invalid choice.");
            }
        }
        scanner.close();
    }
}

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class LibrarySystem {
    private Map<String, Book> books;
    private Map<String, User> users;
    private List<Loan> loans;

    public LibrarySystem() {
        books = new HashMap<>();
        users = new HashMap<>();
        loans = new ArrayList<>();
    }
    public void addBook(String id, String title, String author, int copies) {books.put(id, new Book(id, title, author, copies));}
    public void removeBook(String id) {books.remove(id);}
    public Book findBook(String id) {return books.get(id);}

    public void displayBooks() {
        System.out.println("Library Books:");
        for (Book book : books.values()) {
            System.out.println(book);
        }
    }

    public void addUser(String userId, String name) {users.put(userId, new User(userId, name));}
    public void removeUser(String userId) {users.remove(userId);}
    public User findUser(String userId) {return users.get(userId);}

    public void displayUsers() {
        System.out.println("Library Users:");
        for (User user : users.values()) {
            System.out.println(user);
        }
    }

    public boolean borrowBook(String userId, String bookId) {
        User user = findUser(userId);
        Book book = findBook(bookId);
        if (user != null && book != null && book.getAvailableCopies() > 0) {
            book.borrow();
            Loan loan = new Loan(userId, bookId);
            loans.add(loan);
            user.addLoan(loan);
            return true;
        }
        return false;
    }

    public boolean returnBook(String userId, String bookId) {
        User user = findUser(userId);
        Book book = findBook(bookId);
        if (user != null && book != null) {
            for (Loan loan : loans) {
                if (loan.getUserId().equals(userId) && loan.getBookId().equals(bookId)) {
                    loans.remove(loan);
                    book.returnBook();
                    user.removeLoan(loan);
                    return true;
                }
            }
        }
        return false;
    }

    public void displayLoans() {
        System.out.println("Library Loans:");
        for (Loan loan : loans) {
            System.out.println("User: " + loan.getUserId() + " Book: " + loan.getBookId());
        }
    }

    private static class Book {
        private String id;
        private String title;
        private String author;
        private int copies;
        private int borrowedCopies;

        public Book(String id, String title, String author, int copies) {
            this.id = id;
            this.title = title;
            this.author = author;
            this.copies = copies;
            this.borrowedCopies = 0;
        }

        public String getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public String getAuthor() {
            return author;
        }

        public int getAvailableCopies() {
            return copies - borrowedCopies;
        }

        public void borrow() {
            if (borrowedCopies < copies) {
                borrowedCopies++;
            }
        }

        public void returnBook() {
            if (borrowedCopies > 0) {
                borrowedCopies--;
            }
        }

        @Override
        public String toString() {
            return "Book{" + "id='" + id + '\'' + ", title='" + title + '\'' + ", author='" + author + '\'' + ", availableCopies=" + getAvailableCopies() + '}';
        }
    }

    private static class User {
        private String id;
        private String name;
        private List<Loan> loans;

        public User(String id, String name) {
            this.id = id;
            this.name = name;
            this.loans = new ArrayList<>();
        }
        public String getId() {return id;}
        public String getName() {return name;}
        public void addLoan(Loan loan) {loans.add(loan);}
        public void removeLoan(Loan loan) {loans.remove(loan);}
        public List<Loan> getLoans() {return loans;}

        @Override
        public String toString() {
            return "User{" + "id='" + id + '\'' + ", name='" + name + '\'' + ", loans=" + loans.size() + '}';
        }
    }

    private static class Loan {
        private String userId;
        private String bookId;
        public Loan(String userId, String bookId) {
            this.userId = userId;
            this.bookId = bookId;
        }
        public String getUserId() {return userId;}
        public String getBookId() {return bookId;}
    }

    public static void main(String[] args) {
        LibrarySystem library = new LibrarySystem();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Library System");
            System.out.println("1. Add Book");
            System.out.println("2. Remove Book");
            System.out.println("3. Display Books");
            System.out.println("4. Add User");
            System.out.println("5. Remove User");
            System.out.println("6. Display Users");
            System.out.println("7. Borrow Book");
            System.out.println("8. Return Book");
            System.out.println("9. Display Loans");
            System.out.println("10. Exit");
            System.out.print("Enter choice: ");
            int choice = scanner.nextInt();
            scanner.nextLine();
            if (choice == 1) {
                System.out.print("Enter book ID: ");
                String id = scanner.nextLine();
                System.out.print("Enter book title: ");
                String title = scanner.nextLine();
                System.out.print("Enter book author: ");
                String author = scanner.nextLine();
                System.out.print("Enter number of copies: ");
                int copies = scanner.nextInt();
                library.addBook(id, title, author, copies);
            } else if (choice == 2) {
                System.out.print("Enter book ID: ");
                String id = scanner.nextLine();
                library.removeBook(id);
            } else if (choice == 3) {
                library.displayBooks();
            } else if (choice == 4) {
                System.out.print("Enter user ID: ");
                String userId = scanner.nextLine();
                System.out.print("Enter user name: ");
                String name = scanner.nextLine();
                library.addUser(userId, name);
            } else if (choice == 5) {
                System.out.print("Enter user ID: ");
                String userId = scanner.nextLine();
                library.removeUser(userId);
            } else if (choice == 6) {
                library.displayUsers();
            } else if (choice == 7) {
                System.out.print("Enter user ID: ");
                String userId = scanner.nextLine();
                System.out.print("Enter book ID: ");
                String bookId = scanner.nextLine();
                if (library.borrowBook(userId, bookId)) {
                    System.out.println("Book borrowed successfully.");
                } else {
                    System.out.println("Book could not be borrowed.");
                }
            } else if (choice == 8) {
                System.out.print("Enter user ID: ");
                String userId = scanner.nextLine();
                System.out.print("Enter book ID: ");
                String bookId = scanner.nextLine();
                if (library.returnBook(userId, bookId)) {
                    System.out.println("Book returned successfully.");
                } else {
                    System.out.println("Book could not be returned.");
                }
            } else if (choice == 9) {
                library.displayLoans();
            } else if (choice == 10) {
                System.out.println("Exiting...");
                break;
            } else {
                System.out.println("Invalid choice.");
            }
        }



        scanner.close();
    }
}
