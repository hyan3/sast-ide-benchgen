def hello3(arg1, arg2)
  if arg1
    puts "Hello, world #{arg1}"
    return arg1
  end

  return unless arg2

  puts "Goodbye, world #{arg2}"
  arg2
end
