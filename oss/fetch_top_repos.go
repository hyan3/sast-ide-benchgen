package oss

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/config"
	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/helper"
)

// fetchTopRepos retrieves and clones top trending open-source repositories from GitHub
// based on the specified language and stores them in a configured directory.
func fetchTopRepos(appConfig *config.AppConfig) error {
	// trendy open source project of the past year
	apiURL := fmt.Sprintf(appConfig.GitLabAPIURL, appConfig.Lang)

	// create a dir to store repos
	if err := os.MkdirAll(appConfig.TopOssDir, os.ModePerm); err != nil {
		return fmt.Errorf("failed to create repos directory: %v", err)
	}

	// search for top projects using gitlab api
	log.Debugf("Fetching top repositories from URL %q", apiURL)
	repos, err := searchForTopRepos(apiURL)
	if err != nil {
		return fmt.Errorf("failed to fetch repositories: %v", err)
	}

	// pull the repos
	for _, repo := range repos {
		repoPath := filepath.Join(appConfig.TopOssDir, repo.Name)
		log.Infof("Cloning repository: %q", repo.HTMLURL)

		if _, err := os.Stat(repoPath); os.IsNotExist(err) {
			if err := helper.CloneRepo(repo, repoPath); err != nil {
				return fmt.Errorf("Failed to clone repository %q: %v", repo.HTMLURL, err)
			}
			log.Infof("Successfully cloned %q", repo.HTMLURL)
		} else {
			log.Infof("Local repository path %q already exists, skipping clone...", repoPath)
		}
	}

	return nil
}

func searchForTopRepos(apiURL string) ([]helper.Repo, error) {
	resp, err := http.Get(apiURL)
	if err != nil {
		return nil, fmt.Errorf("error fetching repositories: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("unexpected status code: %d", resp.StatusCode)
	}

	var result []helper.Repo
	if err := json.NewDecoder(resp.Body).Decode(&result); err != nil {
		return nil, fmt.Errorf("error decoding response: %v", err)
	}

	return result, nil
}
