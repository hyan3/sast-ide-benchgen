package oss

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"os"
	"path/filepath"

	sitter "github.com/smacker/go-tree-sitter"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/config"
	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/helper"
)

// generateSeeds extracts language-specific components from source files, saves
// them as seed files, and stores them in the appConfig.SeedFuncsDir.
//
// This function:
// - Creates a directory for seed files if it doesn't exist
// - Retrieves the language configuration based on the provided appConfig
// - Identifies source files with the correct file extension
// - Uses a parser to analyze each source file for specific language constructs (seeds)
// - Saves each extracted component as a new file in the seed directory, naming it based on a SHA-256 hash, for example `9110fd.seed`
// - Stops processing when the maximum number of seeds (`SeedsMax`) is reached
//
// Parameters:
// - appConfig: Configuration for the application, containing language, seed directory, file extension, and other settings
//
// Returns an error if any I/O operation, file parsing, or query execution fails.
func generateSeeds(appConfig *config.AppConfig) error {
	// create seed dir
	if err := os.MkdirAll(appConfig.SeedFuncsDir, os.ModePerm); err != nil {
		return fmt.Errorf("failed to create seed-func directory: %v", err)
	}

	// get all files of the interested language
	langConfig, err := config.GetLanguageConfig(appConfig.Lang)
	if err != nil {
		return err
	}

	sourceFiles, err := helper.GetFilesByExtension(appConfig.TopOssDir, langConfig.Extension())
	if err != nil {
		return fmt.Errorf("failed to get source files with extension %q: %v", langConfig.Extension(), err)
	}

	parser := sitter.NewParser()
	parser.SetLanguage(langConfig.Language())

	seedCount := 0
	for _, file := range sourceFiles {
		content, err := os.ReadFile(file)
		if err != nil {
			return fmt.Errorf("failed to read file %s: %v", file, err)
		}

		tree, err := parser.ParseCtx(context.Background(), nil, content)
		if err != nil {
			return fmt.Errorf("error occured when parsing with treesitter: %v", err)
		}

		rootNode := tree.RootNode()
		cursor := sitter.NewQueryCursor()
		seedQuery, err := langConfig.SeedQuery()
		if err != nil {
			return err
		}
		cursor.Exec(seedQuery, rootNode)

		for {
			// look for a function in the file
			match, ok := cursor.NextMatch()
			if !ok {
				// the node is not a function, skip to the next file
				break
			}

			component := match.Captures[0].Node.Content(content)

			seedCount++
			if seedCount > appConfig.SeedsMax {
				return nil
			}
			hash := sha256.Sum256([]byte(component))
			hashStr := hex.EncodeToString(hash[:])[:6]
			newFileName := filepath.Join(appConfig.SeedFuncsDir, hashStr+".seed")
			if err := os.WriteFile(newFileName, []byte(component), 0640); err != nil {
				return fmt.Errorf("failed to write components to file %s: %v", newFileName, err)
			}
		}
	}

	log.Info("Components have been extracted and saved successfully.")
	return nil
}
