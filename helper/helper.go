package helper

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"
)

// Repo represents an OSS repository.
type Repo struct {
	Name     string `json:"name"`
	FullName string `json:"name_with_namespace"`
	HTMLURL  string `json:"http_url_to_repo"`
}

// GetFilesByExtension returns all files with the specified extension from the given directory.
func GetFilesByExtension(dir, ext string) ([]string, error) {
	var files []string
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return fmt.Errorf("Error walking through dir: %v", err)
		}
		if !info.IsDir() && filepath.Ext(path) == ext {
			files = append(files, path)
		}
		return nil
	})

	return files, err
}

// CloneRepo clones the given repository to the specified local path with a shallow copy (depth=1).
func CloneRepo(repo Repo, clonePath string) error {
	cmd := exec.Command("git", "clone", "--depth=1", repo.HTMLURL, clonePath)
	if output, err := cmd.CombinedOutput(); err != nil {
		return fmt.Errorf("error cloning repository %s: %v\n%s", repo.FullName, err, string(output))
	}
	return nil
}

// FileCount returns the number of files matching the given glob
func FileCount(glob string) (int, error) {
	files, err := filepath.Glob(glob)
	if err != nil {
		return 0, err
	}

	return len(files), nil
}

// CopyFile copies a file from src to dest.
func CopyFile(src, dest string) error {
	in, err := os.Open(src)
	if err != nil {
		return fmt.Errorf("failed to open source file %q: %v", src, err)
	}
	defer in.Close()

	out, err := os.Create(dest)
	if err != nil {
		return fmt.Errorf("failed to create destination file %q: %v", dest, err)
	}
	defer out.Close()

	_, err = io.Copy(out, in)
	if err != nil {
		return fmt.Errorf("failed to copy %q to %q: %v", src, dest, err)
	}

	log.Debugf("Copied %s to %s", src, dest)
	return out.Close()
}
