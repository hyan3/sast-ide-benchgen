package helper

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestGetFilesByExtension(t *testing.T) {
	tempDir := t.TempDir()
	testFiles := []struct {
		name string
		ext  string
	}{
		{"file1.go", ".go"},
		{"file2.go", ".go"},
		{"file3.txt", ".txt"},
	}

	for _, tf := range testFiles {
		_, err := os.Create(filepath.Join(tempDir, tf.name))
		require.NoError(t, err)
	}

	files, err := GetFilesByExtension(tempDir, ".go")
	require.NoError(t, err)
	require.Len(t, files, 2)
}
