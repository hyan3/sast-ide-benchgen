//go:build integration

package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"testing"
	"text/template"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/config"
	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/internal/testutils"
)

// see https://gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/blob/main/config.toml
// for an explanation of what each field represents.
const tomlTemplate = `
BaselineRepoURL = "https://gitlab.com/gitlab-org/security-products/sast-rules.git"
GeneratedFileSizeMax = 20000
GeneratedFileSizeMin = 19000
GitlabAPIURL = "https://gitlab.com/api/v4/projects?with_programming_language=%s&order_by=star_count&simple=true&per_page=3"
Lang = "{{.Lang}}"
NumHybridSamplesToGenerate = 100
NumOssSamplesToGenerate = 100
OutDir = "{{.OutDir}}"
SeedsMax = 1000
SeedTreeHeightMax = 2147483647
SeedTreeHeightMin = 20
TmpDir = "{{.TmpDir}}"
`

func TestMain(t *testing.T) {
	for _, lang := range []string{"go", "java", "python", "ruby"} {
		configPath := createTOMLConfig(t, lang)
		langConfig, err := config.GetLanguageConfig(lang)
		require.NoError(t, err)

		t.Run(fmt.Sprintf("Validate hybrid sample files %s", lang), func(t *testing.T) {
			// step 1: pull baseline samples
			runCommand(t, "baseline", configPath)

			// step 2: generate oss samples
			runCommand(t, "oss", configPath)

			// step 3: generate hybrid samples
			runCommand(t, "hybrid", configPath)

			// step 4: validate that each generated hybrid test file contains the correct number of vulns
			appConfig, err := config.Load(configPath)
			require.NoError(t, err)
			hybridSampleDir := appConfig.GeneratedHybridSamplesDir
			err = filepath.Walk(hybridSampleDir, func(path string, info os.FileInfo, err error) error {
				require.NoError(t, err)

				if !info.IsDir() && filepath.Ext(path) == langConfig.Extension() {
					content := testutils.ReadFile(t, path)
					// In some rare cases, the baseline sample is just a placeholder and contains no vuln.
					if config.SastRulesPlaceholderRegex.Match([]byte(content)) {
						t.Logf("Skipping validation for file %s due to TODO: Placeholder", path)
						return nil
					}
					// count the occurrences of "ruleid" in the file, representing vulns
					vulnCount := len(config.RuleidRegex.FindAll([]byte(content), -1))
					require.Equal(t, appConfig.NumVulnerabilities, vulnCount)
				}
				return nil
			})

			require.NoError(t, err)
		})
	}
}

func createTOMLConfig(t *testing.T, lang string) string {
	tempDir := testutils.TempDir(t)
	t.Logf("Using tempdir %q", tempDir)

	tmpl, err := template.New("test").Parse(tomlTemplate)
	require.NoError(t, err)

	tomlFilePath := filepath.Join(tempDir, lang+".toml")

	file, err := os.Create(tomlFilePath)
	require.NoError(t, err)
	defer file.Close()

	err = tmpl.Execute(file, map[string]string{"Lang": lang, "TmpDir": tempDir, "OutDir": tempDir})
	require.NoError(t, err)

	return tomlFilePath
}

func runCommand(t *testing.T, commandName, configPath string) {
	cmd := exec.Command("go", "run", ".", commandName, "-c", configPath)
	require.NoError(t, cmd.Run())
}
