package main

import (
	"fmt"
	"os"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/config"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/baseline"
	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/hybrid"
	"gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen/oss"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/logutil"
)

var configFlag = cli.StringFlag{
	Name:     "config",
	Aliases:  []string{"c"},
	Usage:    "Path to config file (default: config.toml)",
	Value:    "config.toml",
	Required: false,
}

var languageFlag = cli.StringFlag{
	Name:     "language",
	Aliases:  []string{"lang"},
	Usage:    "Specify language for generated samples (allowed values: 'python', 'ruby', 'go', 'java')",
	Required: false,
}

var programSizeFlag = cli.StringFlag{
	Name:     "program_size",
	Aliases:  []string{"size"},
	Usage:    "Specify program size: 'large' or 'small'",
	Required: false,
}

var vulnLevelFlag = cli.StringFlag{
	Name:     "vuln_level",
	Aliases:  []string{"vulns"},
	Usage:    "Specify vulnerability level: 'high' or 'low'",
	Required: false,
}

func main() {
	log.SetFormatter(&logutil.Formatter{Project: "Benchgen"})

	app := &cli.App{
		Name:  "benchgen",
		Usage: "Run baseline, oss, hybrid in sequence",
		Commands: []*cli.Command{
			{
				Name:    "baseline",
				Aliases: []string{"bl"},
				Usage:   "Pull the baseline samples from https://gitlab.com/gitlab-org/security-products/sast-rules",
				Flags:   []cli.Flag{&configFlag, &languageFlag, &programSizeFlag, &vulnLevelFlag},
				Action: func(c *cli.Context) error {
					return executeAction(c, baseline.Fetch)
				},
			},
			{
				Name:    "oss",
				Aliases: []string{"os"},
				Usage:   "Generate oss samples",
				Flags:   []cli.Flag{&configFlag, &languageFlag, &programSizeFlag, &vulnLevelFlag},
				Action: func(c *cli.Context) error {
					return executeAction(c, oss.Generate)
				},
			},
			{
				Name:    "hybrid",
				Aliases: []string{"hy"},
				Usage:   "Generate hybrid samples",
				Flags:   []cli.Flag{&configFlag, &languageFlag, &programSizeFlag, &vulnLevelFlag},
				Action: func(c *cli.Context) error {
					return executeAction(c, hybrid.GenSamples)
				},
			},
		},
	}

	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func executeAction(c *cli.Context, generateFunc func(*config.AppConfig) error) error {
	configPath := c.String("config")
	appConfig, err := config.Load(configPath)
	if err != nil {
		return err
	}

	if c.IsSet("lang") {
		appConfig.Lang = c.String("lang")
	}
	size := c.String("size")
	vulns := c.String("vulns")
	if size != "" && vulns != "" {
		configValues, ok := config.Configurations[appConfig.Lang][size][vulns]
		if ok {
			appConfig.NumVulnerabilities = configValues.NumVulnerabilities
			appConfig.LinesOfCodeToGenerate = configValues.LinesOfCodeToGenerate
		} else {
			return fmt.Errorf("invalid configuration for lang: %s, size: %s, vulns: %s", appConfig.Lang, size, vulns)
		}
	}

	return generateFunc(appConfig)
}
