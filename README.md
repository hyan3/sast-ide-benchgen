# sast-ide-benchgen

This project aims to generate tests for SAST IDE tools.

(It is a personal project first, and will be transferred to the group's namespace when ready.)

## How to use
Just **trigger the pipeline**, and collect the benchmark from
the artifacts of the `pull baseline tests` job,
the `generate oss tests` job
and the `generate hybrid tests` job.

Alternatively, clone the repo to your local machine,
and run `go run cmd/main.go baseline`,
`go run cmd/main.go oss`
and `go run cmd/main.go hybrid` locally,
and collect the benchmark from
`baseline_tests` dir,
`generated_oss_tests` dir
and `generated_hybrid_tests` dir, respectively.

## How to create representative sample sets
You can specify the characteristics of the generated test set through command-line options,
including the language, program size, and vulnerability density.
These options are defined in [`config/data.go`](./config/data.go) and take priority over any settings in [`config.toml`](./config.toml).

`language` (`--lang`): Specifies the programming language for the generated samples. Supported languages are python, ruby, go, and java.
`program size` (`--size`): Defines the scale of the program, with options large or small.
`vuln_level` (`--vulns`): Sets the density of vulnerabilities in the test set, with high indicating a high density and low indicating a low density.

For example, you can run the following commands to generate baseline, open-source, and hybrid samples:

`go run cmd/main.go baseline --lang python --vulns high --size large`,
`go run cmd/main.go oss --lang python --vulns high --size large`,
`go run cmd/main.go hybrid --lang python --vulns high --size large`.

In these commands:

`baseline`, `oss`, and `hybrid` specify different generation modes.
`--lang python`, `--vulns high`, and `--size large` customize the test set's language, vulnerability density, and size.

## How to run integration test
Run `go test --tags=integration ./...`.

This will pull baseline samples, generate OSS and hybrid samples, and validate
that each generated hybrid test file contains the correct number of vulns.

## How it works
```
 __________________        ________________       
|                  |      |                |      
| sast-rules tests |      |  top OSS repos |
|__________________|      |________________|
                \                 |
                 \                |
                  \        ______\|/_______       
                   \      |                |      
                    \     | function seeds |
                     \    |________________|
                      \     /  
                       \   /
                  ______\|/_______       
                 |                |      
                 |  hybrid tests  |
                 |________________|
```
The benchmark consists of **three parts**:
* The unit tests from [the sast-rules repo](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tree/31680b796541f37be581a8fdf3ea42d09b978a6f/go)
  * This part is to for the recall
* The syntax-correct snippets (a bunch of functions) from [top oss repos](config.toml#L33)
  * This part is to real-world programs
  * It allows customisation by specifying parameters in [config.toml](config.toml)
* The syntax-correct hybrid code combining the above two
  * This part is to mimic arbitrary real-world programs that contains vulns (that can be captured by [sast-rules](https://gitlab.com/gitlab-org/security-products/sast-rules/-/tree/31680b796541f37be581a8fdf3ea42d09b978a6f/go)).

